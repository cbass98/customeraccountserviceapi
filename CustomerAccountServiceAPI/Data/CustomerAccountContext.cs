﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using CustomerAccountServiceAPI.Models;

namespace CustomerAccountServiceAPI.Data
{
    public class CustomerAccountContext : DbContext
    {
        public CustomerAccountContext (DbContextOptions<CustomerAccountContext> options)
            : base(options)
        {
        }

        public DbSet<CustomerAccountServiceAPI.Models.CustomerAccount> CustomerAccount { get; set; }
    }
}
