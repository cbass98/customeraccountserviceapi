﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using CustomerAccountServiceAPI.Data;

namespace CustomerAccountServiceAPI.Models
{
    public class PrepDB
    {
        public static void PrepPopulation(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                SeedData(serviceScope.ServiceProvider.GetService<CustomerAccountContext>());
            }
        }
        public static void SeedData(CustomerAccountContext context)
        {
            System.Console.WriteLine("Applying Migrations...");

            context.Database.Migrate();
        }
    }
}
