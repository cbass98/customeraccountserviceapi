﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerAccountServiceAPI.Models
{
    public class Review
    {
        public long Id { get; set; }
        public long CustomerId { get; set; }
        public long ProductId { get; set; }
        public int Rating { get; set; }
        public string Comments { get; set; }
        public bool IsVisible { get; set; }
    }
}
