﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CustomerAccountServiceAPI.Data;
using CustomerAccountServiceAPI.Models;

namespace CustomerAccountServiceAPI.Controllers
{
    [Route("api/customeraccounts")]
    [ApiController]
    public class CustomerAccountsController : ControllerBase
    {
        private readonly CustomerAccountContext _context;

        public CustomerAccountsController(CustomerAccountContext context)
        {
            _context = context;
        }

        // GET: api/CustomerAccounts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CustomerAccount>>> GetCustomerAccount()
        {
            return await _context.CustomerAccount.ToListAsync();
        }

        // GET: api/CustomerAccounts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerAccount>> GetCustomerAccount(long id)
        {
            var customerAccount = await _context.CustomerAccount.FindAsync(id);

            if (customerAccount == null)
            {
                return NotFound();
            }

            return customerAccount;
        }

        // PUT: api/CustomerAccounts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCustomerAccount(long id, CustomerAccount customerAccount)
        {
            if (id != customerAccount.Id)
            {
                return BadRequest();
            }

            _context.Entry(customerAccount).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CustomerAccountExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CustomerAccounts
        [HttpPost]
        public async Task<ActionResult<CustomerAccount>> PostCustomerAccount(CustomerAccount customerAccount)
        {
            _context.CustomerAccount.Add(customerAccount);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCustomerAccount", new { id = customerAccount.Id }, customerAccount);
        }

        // DELETE: api/CustomerAccounts/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CustomerAccount>> DeleteCustomerAccount(long id)
        {
            var customerAccount = await _context.CustomerAccount.FindAsync(id);
            if (customerAccount == null)
            {
                return NotFound();
            }

            _context.CustomerAccount.Remove(customerAccount);
            await _context.SaveChangesAsync();

            return customerAccount;
        }

        private bool CustomerAccountExists(long id)
        {
            return _context.CustomerAccount.Any(e => e.Id == id);
        }
    }
}
